
package com.creditcard.test.repositories;

import com.creditcard.test.entities.Response;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResponseRepository extends JpaRepository<Response, String>{
    
}
