
package com.creditcard.test.repositories;

import com.creditcard.test.entities.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends JpaRepository <Card, String> {
    
    public Card findByNameAndLastName(String name, String lastName);
    
    public Card findByNumber(String number);
}
