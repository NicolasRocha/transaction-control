package com.creditcard.test.services;

import com.creditcard.test.entities.Card;
import com.creditcard.test.repositories.CardRepository;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardService {
    
    private CardRepository cardRepository;
    
    @Autowired
    public CardService(CardRepository cardRepository){
        this.cardRepository = cardRepository;
    }
    
    public boolean validCard(Card card){
        Calendar today = new GregorianCalendar();
        return card.getExpiration().after(today.getTime());
    }
    
    public boolean difCard(Card card1, Card card2){
        return card1.equals(card2);
    }
    
    @Transactional
    public List <Card> findAll(){
        return cardRepository.findAll();
    }
    
    @Transactional
    public Card findByHolder(String name, String lastName){
        return cardRepository.findByNameAndLastName(name, lastName);
    }
    
    
    @Transactional
    public Card createCard(Card card){
        return cardRepository.save(card);
    }
    
    public Card findByNumber(String number){
        
        return cardRepository.findByNumber(number);
    }
    
}
