package com.creditcard.test.services;

import com.creditcard.test.entities.Response;
import com.creditcard.test.repositories.ResponseRepository;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {
    
    private ResponseRepository responseRepository;
    
    @Autowired
    public TransactionService(ResponseRepository responseRepository){
        this.responseRepository = responseRepository;
    }
    
    Calendar today = new GregorianCalendar();
    final int DAY = today.get(Calendar.DAY_OF_MONTH);
    final int MONTH = today.get(Calendar.MONTH);
    final int YEAR = (today.get(Calendar.YEAR)) % 2000;
    
    private boolean validOperation(Response transaction){
        return (transaction.getCost() <= 1000);
    }
    
    public float getTransactionFee(Response transaction){
        System.out.println(transaction.getCard().getBrand());
        
        if (validOperation(transaction)) {
            float fee = calculateFee(transaction);
            transaction.setFee(fee);
            return fee;
        }
        return -1;
    }
    
    private float calculateFee(Response transaction){
        System.out.println("Hola mundo");
        
        String brand = transaction.getCard().getBrand();
        System.out.println(brand);
        
        switch (brand) {
            case "VISA":
                return (YEAR/MONTH) *VISAConstant();
            case "NARA":
                return (float)(DAY * 0.5 * NARAConstant());
            case "AMEX":
                return (float) (MONTH* 0.1 *AMEXConstant());
        }
        return -1;
    }
    
    /**
     * Gets a constant which ensures that the rate for the VISA card will be
     * equaly split along the year, and will always be between 0,3 and 5
     * @return 
     */
    private float VISAConstant(){
        float min = (float) (3.6/YEAR);
        float max = (float) (5/YEAR);
        
        float interval = (max - min) / 12;
        
        float constant = min + ((12 -(MONTH+1)) * interval);
        return constant;
    }
    
    /**
     * Gets a constant which ensures that the rate for the NARA card will be
     * equaly split along the year, and will always be between 0,3 and 5
     * @return 
     */
    private float NARAConstant(){
        float min = 0.6f;
        float max = 10/31;
        
        float interval = (max - min) /31;
        float constant = min + ((DAY-1) * interval);
        
        return constant;
        
    }

     /**
     * Gets a constant which ensures that the rate for the AMEX card will be
     * equaly split along the year, and will always be between 0,3 and 5
     * @return 
     */
    private float AMEXConstant(){
        float min = 3f;
        float max = 50 / 12;
        
        float interval = (max - min) / 11;
        
        float constant = min + (MONTH * interval);
        return constant;
    }
    
    public List<Response> findAll(){
        return responseRepository.findAll();
    }
    
    public Response createResponse(Response response){
        return responseRepository.save(response);
    }
}
