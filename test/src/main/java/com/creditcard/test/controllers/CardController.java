package com.creditcard.test.controllers;

import com.creditcard.test.entities.Card;
import com.creditcard.test.services.CardService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class CardController {
        
    private CardService cardService;
    
    @Autowired
    public CardController(CardService cardService){
        this.cardService = cardService;
    }
    
    @GetMapping("/cards")
    public List<Card> findAll(){
        return cardService.findAll();
    }
    
    @RequestMapping(value="/card", method=RequestMethod.GET)
    public Card findByHolder(@RequestParam("name") String name,
            @RequestParam("lastName") String lastName){
        
        return cardService.findByHolder(name, lastName);
    }
    
    @RequestMapping(value="/card", method=RequestMethod.POST)
    public Card createCard(@RequestBody Card card){
        return cardService.createCard(card);
    }
    
    @RequestMapping(value="/getCard", method=RequestMethod.GET)
    public Card findByNumber(@RequestParam("number") String number){
        
        return cardService.findByNumber(number);
    }
}
