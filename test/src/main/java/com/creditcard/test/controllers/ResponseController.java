package com.creditcard.test.controllers;

import com.creditcard.test.entities.Card;
import com.creditcard.test.entities.Response;
import com.creditcard.test.services.TransactionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/response")
public class ResponseController {
    
    private TransactionService transactionService;
    private CardController cardController;
    
    @Autowired
    public ResponseController(TransactionService transactionService, CardController cardController){
        this.transactionService = transactionService;
        this.cardController = cardController;
    }
    
    @RequestMapping(value = "/responses", method = RequestMethod.GET)
    public List<Response> findAll(){
        return transactionService.findAll();
    }
    
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Response createResponse(@RequestBody Response response){
        
        if (cardController.findByNumber(response.getCard().getNumber())== null) {
            cardController.createCard(response.getCard());
        }
        float fee = transactionService.getTransactionFee(response);
        if (fee == -1) {
            return new Response();
        }
        response.setFee(fee);
        return transactionService.createResponse(response);
    }

}
