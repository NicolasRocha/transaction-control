package com.creditcard.test.entities;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Card {
    
    @Id
    private String number;
    
    private String brand;
    private String name;
    private String lastName;
    
    @DateTimeFormat
    private Date expiration;
    
}
